---
# file: 03_setup_k8s_platform/tasks/context_management-stack/install_orion.yml

- name: Set MongoDB internal endpoint
  ansible.builtin.set_fact:
    orion_mongodb_endpoint: "orion-mongodb.{{ k8s_namespace }}"
  when: inv_mngd_db.orion_mongo.enable == "false"

- name: Check if MongoDB internal endpoint exists
  kubernetes.core.k8s_info:
    kubeconfig: "{{ k8s_config }}"
    context: "{{ k8s_context }}"
    namespace: "{{ k8s_namespace }}"
    kind: Service
    name: "orion-mongodb"
  register: orion_mongodb_endpoint_exists
  when: inv_mngd_db.orion_mongo.enable == "false"

- name: Check for MongoDB internal endpoint failed
  fail:
    msg: "Internal endpoint {{ orion_mongodb_endpoint }} does not exist!"
  when: orion_mongodb_endpoint_exists['resources'][0] is undefined and inv_mngd_db.orion_mongo.enable == "false"

- name: Set variables by managed MongoDB
  block:
    - name: Get MongoDB user credentials from secret
      kubernetes.core.k8s_info:
        kind: Secret
        namespace: '{{ inv_cm.fiware.context_mgmt_ns_name }}'
        kubeconfig: '{{ k8s_config }}'
        context: "{{ k8s_context }}"
        name: '{{ db_common.orion_mongo.user.k8s_secret }}'
      register: orion_mongo_user

    - name: Set MongoDB variables for Orion deployment
      ansible.builtin.set_fact:
        orion_mongodb_username: "{{ orion_mongo_user.resources[0].data.MONGO_USERNAME | b64decode }}"
        orion_mongodb_password: "{{ orion_mongo_user.resources[0].data.MONGO_PASSWORD | b64decode }}"
        orion_mongodb_endpoint: "{{ inv_mngd_db.orion_mongo.db_address }}"
        orion_mongodb_replicaset_flag: "rs0"
  when: inv_mngd_db.orion_mongo.enable == "true"

- name: Set variables by self deployed MongoDB
  ansible.builtin.set_fact:
    orion_mongodb_username: "{{ inv_cm.mongo.initdb_root_username }}"
    orion_mongodb_password: "{{ inv_cm.mongo.initdb_root_password }}"
    orion_mongodb_replicaset_flag: ""
  when: inv_mngd_db.orion_mongo.enable == "false"

- name: Deploy Orion
  no_log: '{{ ansible_debug.no_log }}'
  kubernetes.core.k8s:
    state: present
    kubeconfig: "{{ k8s_config }}"
    context: "{{ k8s_context }}"
    namespace: "{{ k8s_namespace }}"
    definition: "{{ item }}"
  loop:
    - "{{ lookup('template', 'templates/08_context_management/orion.yml') | from_yaml_all | list }}"

- name: Wait till deployment of Orion is completed
  kubernetes.core.k8s_info:
    kubeconfig: "{{ k8s_config }}"
    context: "{{ k8s_context }}"
    namespace: "{{ k8s_namespace }}"
    kind: Pod
    label_selectors:
      - app=orion
    wait: yes
    wait_sleep: 10
    wait_timeout: 360
    wait_condition:
      type: Ready
      status: True
