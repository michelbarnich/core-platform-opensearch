---
- name: Verify MongoDB credentials are set
  ansible.builtin.fail:
    msg: "ERROR: inventory variable {{ item.key | upper }} is not set."
  when: item.value|length  == 0 and inv_mngd_db.orion_mongo.enable == "false"
  with_items:
    - { key: inv_cm.mongo.initdb_database, value: "{{ inv_cm.mongo.initdb_database }}"}
    - { key: inv_cm.mongo.initdb_root_username, value: "{{ inv_cm.mongo.initdb_root_username }}" }
    - { key: inv_cm.mongo.initdb_root_password, value: "{{ inv_cm.mongo.initdb_root_password }}" }
    - { key: inv_cm.orion.mongodb_user, value: "{{ inv_cm.orion.mongodb_user }}" }
    - { key: inv_cm.orion.mongodb_password, value: "{{ inv_cm.orion.mongodb_password }}" }
  no_log: '{{ ansible_debug.no_log }}'

- name: Set default facts for K8S
  ansible.builtin.set_fact:
    k8s_config: "{{ K8S_KUBECONFIG_PATH }}/{{ inv_cm.fiware.ns_kubeconfig }}"
    k8s_context: "{{ inv_k8s.config.context }}"
    k8s_namespace: "{{ inv_cm.fiware.context_mgmt_ns_name | lower }}"

# 1. Create namespace for CM stack
- name: "Create namespace {{ k8s_namespace }}"
  kubernetes.core.k8s:
    state: present
    kubeconfig: "{{ k8s_config }}"
    context: "{{ k8s_context }}"
    definition:
      api_version: v1
      kind: Namespace
      metadata:
        name: "{{ k8s_namespace }}"
        labels:
          name: "{{ k8s_namespace }}"
  when: inv_cm.fiware.context_mgmt_ns_create == "true"

# 2. Install MongoDB
- name: Install MongoDB
  import_tasks: tasks/08_context_management/install_mongodb.yml
  tags: mongodb
  when: inv_mngd_db.orion_mongo.enable == "false"

# 3. Install Quantum Leap
- name: Install Quantum Leap
  import_tasks: tasks/08_context_management/install_quantumleap.yml
  tags: quantumleap

# 4. Install Orion
- name: Install Orion
  import_tasks: tasks/08_context_management/install_orion.yml
  tags: orion

# 5. Setup velero backup
- name: "Create velero Backup schedule namespace {{ k8s_namespace }}"
  kubernetes.core.k8s:
    state: present
    kubeconfig: "{{ k8s_config }}"
    context: "{{ k8s_context }}"
    namespace: "{{ inv_op_stack.velero.ns_name }}"
    definition:
      apiVersion: velero.io/v1
      kind: Schedule
      metadata:
        name: "{{ k8s_namespace }}-backup"
      spec:
        schedule: 0 2 * * *
        template:
          defaultVolumesToRestic: true
          hooks: {}
          includedNamespaces:
            - '{{ k8s_namespace }}'
          metadata: {}
          snapshotVolumes: true
          storageLocation: "{{ inv_op_stack.velero.backup.location_name }}-backup"
          ttl: 720h0m0s
  when: inv_op_stack.velero.enable == "true"
