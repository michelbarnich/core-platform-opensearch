// Scans for the groups marked as organisations, and constructs an object to list the
// subgroups marked as user roles

var HashMap = Java.type('java.util.HashMap');
var HashSet = Java.type('java.util.HashSet');
var ArrayList = Java.type('java.util.ArrayList');

var client = keycloakSession.getContext().getClient();
var rm = user.getClientRoleMappings(client)
var roles = user.getGroups();
var orgs = new ArrayList();

roles.forEach(scanOrgs);
rm.forEach(scanSysAdmin)

function scanOrgs(group){
    // is scanned group role
    if ( isRole(group)===true ) {
        // and parent is an org
        if(isOrg(group.getParent())) {
            var org = new HashMap();
            org.put("org",group.getParent().getName());
            org.put("role",group.getName());
            orgs.add(org);
        }
    }
}

function scanSysAdmin(mapping){
    var adminRole = "ckanAdmin";
    if(mapping.getName() == adminRole) {
        orgs.add(adminRole)
    }
}


function getRoot(group){
    if(group.getParent() !== null)
        return getRoot(group.getParent());
    return group;
}


function isRole(group){
    if (group.getFirstAttribute("role") == "true")
        return true;
    return false;
}

function isOrg(group){
    if (group.getParent()===null &&
        group.getFirstAttribute("org") == "true"){
        return true;
    }
    return false;
}

token.setOtherClaims("ckan_roles", orgs);